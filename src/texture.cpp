#include "raytracer.h"
#include "texture.h"
#include <gli/gli.hpp>
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

Texture::Texture(VDeleter<VkDevice> &device, VkPhysicalDevice physicalDevice,
        VkCommandPool commandPool, VkQueue queue, uint32_t width, uint32_t height,
        uint32_t binding, VkFormat format)
    : device(device),
    binding(binding),
    type(VK_DESCRIPTOR_TYPE_STORAGE_IMAGE) {

    checkSupport(physicalDevice, format);
    initDestructors();

    createInfo(width, height, format, VK_IMAGE_TILING_OPTIMAL);
    allocateMemory(physicalDevice);
    createImageBarrier(commandPool, queue, VK_IMAGE_LAYOUT_GENERAL);
    createSampler();
    createImageView(format);
}

Texture::Texture(VDeleter<VkDevice> &device, VkPhysicalDevice physicalDevice,
        VkCommandPool commandPool, VkQueue queue, const char *fName,
        uint32_t binding, VkFormat format)
    : device(device),
    binding(binding),
    type(VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER) {

    checkSupport(physicalDevice, format);
    initDestructors();

    /* gli::texture2d tex2D(gli::load(fName)); */
    int width, height, texChannels;
    stbi_uc* pixels = stbi_load(fName, &width, &height, &texChannels, STBI_rgb_alpha);
    VkDeviceSize imageSize = width * height * 4;

    if (!pixels) {
        throw std::runtime_error("failed to load texture image!");
    }
    /* printf("Loading textures with %d levels\n", tex2D.levels()); */

    /* auto width = (uint32_t) tex2D[0].extent().x; */
    /* auto height = (uint32_t) tex2D[0].extent().y; */
    /* auto mipLevels = (uint32_t) tex2D.levels(); */

    VkMemoryAllocateInfo memAllocInfo = {};
    memAllocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    VkMemoryRequirements memReqs;

    /* create command buffer for texture loading */
    VkCommandBufferAllocateInfo cmdBuffAllocateInfo = {};
    cmdBuffAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    cmdBuffAllocateInfo.commandPool = commandPool;
    cmdBuffAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    cmdBuffAllocateInfo.commandBufferCount = 1;

    VkCommandBuffer cmdBuffer;
    if (vkAllocateCommandBuffers(device, &cmdBuffAllocateInfo, &cmdBuffer)
            != VK_SUCCESS) {
        throw std::runtime_error("error");
    }

    VkCommandBufferBeginInfo cmdBufInfo = {};
    cmdBufInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    if (vkBeginCommandBuffer(cmdBuffer, &cmdBufInfo) != VK_SUCCESS) {
        throw std::runtime_error("error");
    }

    /* Create a host-visible staging buffer that contains the raw image data */
    VkBuffer stagingBuffer;
    VkDeviceMemory stagingMemory;

    VkBufferCreateInfo bufferCreateInfo = {};
    bufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferCreateInfo.size = imageSize;//tex2D.size();

    /* This buffer is used as a transfer source for the buffer copy */
    bufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
    bufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    if (vkCreateBuffer(device, &bufferCreateInfo, nullptr, &stagingBuffer)
            != VK_SUCCESS) {
        throw std::runtime_error("error");
    }

    /*
     * Get memory requirements for the staging buffer
     * (alignment, memory type bits)
     */
    vkGetBufferMemoryRequirements(device, stagingBuffer, &memReqs);

    memAllocInfo.allocationSize = memReqs.size;
    // Get memory type index for a host visible buffer
    memAllocInfo.memoryTypeIndex = findMemoryType(physicalDevice,
            memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
            | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

    if (vkAllocateMemory(device, &memAllocInfo, nullptr, &stagingMemory)
            != VK_SUCCESS) {
        throw std::runtime_error("error");
    }

    if (vkBindBufferMemory(device, stagingBuffer, stagingMemory, 0)
            != VK_SUCCESS) {
        throw std::runtime_error("error");
    }

    /* Copy texture data into staging buffer */
    uint8_t *data;
    if (vkMapMemory(device, stagingMemory, 0, memReqs.size, 0, (void **)&data)
            != VK_SUCCESS) {
        throw std::runtime_error("error");
    }

    memcpy(data, pixels, (size_t) imageSize);

    vkUnmapMemory(device, stagingMemory);

    /* Setup buffer copy regions for each mip level */
    std::vector<VkBufferImageCopy> bufferCopyRegions;

    VkBufferImageCopy bufferCopyRegion = {};
    bufferCopyRegion.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    bufferCopyRegion.imageSubresource.mipLevel = 0;
    bufferCopyRegion.imageSubresource.baseArrayLayer = 0;
    bufferCopyRegion.imageSubresource.layerCount = 1;
    bufferCopyRegion.imageExtent.width = width;
    bufferCopyRegion.imageExtent.height = height;
    bufferCopyRegion.imageExtent.depth = 1;
    bufferCopyRegion.bufferOffset = 0;

    bufferCopyRegions.push_back(bufferCopyRegion);


    // Create optimal tiled target image
    printf("Create 1\n");
    VkImageCreateInfo imageCreateInfo = {};
    imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
    imageCreateInfo.format = format;
    imageCreateInfo.mipLevels = 1;//mipLevels;
    imageCreateInfo.arrayLayers = 1;
    imageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
    imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
    // TODO: Do we use the same queue for transfer and compute?
    imageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    imageCreateInfo.extent = { (uint32_t) width, (uint32_t) height, 1 };
    imageCreateInfo.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT
        | VK_IMAGE_USAGE_SAMPLED_BIT;

    if (vkCreateImage(device, &imageCreateInfo, nullptr, image.replace())
            != VK_SUCCESS) {
        throw std::runtime_error("error");
    }

    vkGetImageMemoryRequirements(device, image, &memReqs);

    memAllocInfo.allocationSize = memReqs.size;

    memAllocInfo.memoryTypeIndex = findMemoryType(physicalDevice,
            memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
    if (vkAllocateMemory(device, &memAllocInfo, nullptr,
                memory.replace()) != VK_SUCCESS) {
        throw std::runtime_error("error");
    }
    if (vkBindImageMemory(device, image, memory, 0)
            != VK_SUCCESS) {
        throw std::runtime_error("error");
    }

    VkImageSubresourceRange subresourceRange = {};
    subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    subresourceRange.baseMipLevel = 0;
    subresourceRange.levelCount = 1;//mipLevels;
    subresourceRange.layerCount = 1;

    // Image barrier for optimal image (target)
    // Optimal image will be used as destination for the copy
    setImageLayout(cmdBuffer, image,VK_IMAGE_ASPECT_COLOR_BIT,
            VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            subresourceRange);

    /* Copy mip levels from staging buffer */
    vkCmdCopyBufferToImage(
            cmdBuffer,
            stagingBuffer,
            image,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            static_cast<uint32_t>(bufferCopyRegions.size()),
            bufferCopyRegions.data()
            );

    /* Change texture image layout to shader read */
    setImageLayout(
            cmdBuffer,
            image,
            VK_IMAGE_ASPECT_COLOR_BIT,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
            subresourceRange);

    /* flush command buffer */
    if (vkEndCommandBuffer(cmdBuffer) != VK_SUCCESS) {
        throw std::runtime_error("error");
    }

    VkSubmitInfo submitInfo = {};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &cmdBuffer;

    // TODO: Isn't this equivalent to a VkWaitQueueIdle?

    /* Create fence to ensure that the command buffer has finished executing */
    VkFenceCreateInfo fenceCreateInfo {};
    fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceCreateInfo.flags = 0;

    // TOOD: Isn't this equivalent to just calling QueueWaitIdle?
    VkFence fence;
    if (vkCreateFence(device, &fenceCreateInfo, nullptr, &fence) != VK_SUCCESS) {
        throw std::runtime_error("error");
    }

    /* Submit to the queue */
    if (vkQueueSubmit(queue, 1, &submitInfo, fence) != VK_SUCCESS) {
        throw std::runtime_error("error");
    }

    // Wait for the fence to signal that command buffer has finished executing
    if (vkWaitForFences(device, 1, &fence, VK_TRUE, 100000000000)
            != VK_SUCCESS) {
        throw std::runtime_error("error");
    }

    vkDestroyFence(device, fence, nullptr);
    vkFreeCommandBuffers(device, commandPool, 1, &cmdBuffer);
    vkFreeMemory(device, stagingMemory, nullptr);
    vkDestroyBuffer(device, stagingBuffer, nullptr);

    createSampler();
    createImageView(format);
}

VkDescriptorSetLayoutBinding Texture::getDescriptorSetLayoutBinding() {
    return { binding,  type, 1, VK_SHADER_STAGE_COMPUTE_BIT, nullptr };
}

VkWriteDescriptorSet Texture::getWriteDescriptorSet(VkDescriptorSet dstSet) {
    VkWriteDescriptorSet writeDescriptorSet {};
    writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    writeDescriptorSet.dstSet = dstSet;
    writeDescriptorSet.descriptorType = type;
    writeDescriptorSet.dstBinding = binding;
    writeDescriptorSet.pImageInfo = &descriptor;
    writeDescriptorSet.descriptorCount = 1;
    return writeDescriptorSet;
}

void Texture::checkSupport(VkPhysicalDevice physicalDevice, VkFormat format) {
    /* TODO factor outside to amortize over multiple textures */
    VkFormatProperties formatProps;
    vkGetPhysicalDeviceFormatProperties(physicalDevice, format,
            &formatProps);
    if ((!formatProps.optimalTilingFeatures) &
            VK_FORMAT_FEATURE_STORAGE_IMAGE_BIT) {
        throw std::runtime_error("device doesn't support image storage");
    }
}

void Texture::initDestructors() {
    view = VDeleter<VkImageView>(device, vkDestroyImageView);
    image = VDeleter<VkImage>(device, vkDestroyImage);
    sampler = VDeleter<VkSampler>(device, vkDestroySampler);
    memory = VDeleter<VkDeviceMemory>(device, vkFreeMemory);
}

void Texture::createInfo(uint32_t width, uint32_t height, VkFormat format,
        VkImageTiling tiling) {
    VkImageCreateInfo imageCreateInfo = {};

    printf("Create 2\n");

    imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
    imageCreateInfo.format = format;
    imageCreateInfo.extent = { width, height, 1};
    imageCreateInfo.mipLevels = 1;
    imageCreateInfo.arrayLayers = 1;
    imageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
    imageCreateInfo.tiling = tiling;
    imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    imageCreateInfo.flags = 0;
    imageCreateInfo.usage = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_STORAGE_BIT;

    if (vkCreateImage(device, &imageCreateInfo, nullptr, image.replace())
            != VK_SUCCESS) {
        throw std::runtime_error("failed to create texture");
    }
}

VkDeviceSize Texture::allocateMemory(VkPhysicalDevice physicalDevice) {
    VkMemoryAllocateInfo memAlloc = {};
    memAlloc.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;

    VkMemoryRequirements memReqs;

    vkGetImageMemoryRequirements(device, image, &memReqs);
    memAlloc.allocationSize = memReqs.size;
    memAlloc.memoryTypeIndex = findMemoryType(physicalDevice,
            memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

    if (vkAllocateMemory(device, &memAlloc, nullptr, memory.replace())
            != VK_SUCCESS) {
        throw std::runtime_error("failed to allocate texture memory");
    }

    if (vkBindImageMemory(device, image, memory, 0) != VK_SUCCESS) {
        throw std::runtime_error("failed to bind texture memory");
    }

    return memReqs.size;
}

VkCommandBuffer Texture::startCommand(VkCommandPool commandPool) {
    VkCommandBuffer cmd;

    VkCommandBufferAllocateInfo cmdBufferAlloc = {};
    cmdBufferAlloc.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    cmdBufferAlloc.commandPool = commandPool;
    cmdBufferAlloc.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    cmdBufferAlloc.commandBufferCount = 1;

    if (vkAllocateCommandBuffers(device, &cmdBufferAlloc, &cmd)
            != VK_SUCCESS) {
        throw std::runtime_error("failed to create command buffer");
    }

    /* start command */
    VkCommandBufferBeginInfo cmdBuffInfo = {};
    cmdBuffInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    if (vkBeginCommandBuffer(cmd, &cmdBuffInfo) != VK_SUCCESS) {
        throw std::runtime_error("failed to start command");
    }

    return cmd;
}

void Texture::endCommand(VkCommandBuffer cmd, VkCommandPool commandPool,
        VkQueue queue) {
    /* flush command */
    if (vkEndCommandBuffer(cmd) != VK_SUCCESS) {
        throw std::runtime_error("failed to end command");
    }

    VkSubmitInfo submitInfo = {};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &cmd;

    if (vkQueueSubmit(queue, 1, &submitInfo,
                VK_NULL_HANDLE) != VK_SUCCESS) {
        throw std::runtime_error("failed to submit command to queue");
    }
    if (vkQueueWaitIdle(queue) != VK_SUCCESS) {
        throw std::runtime_error("error on command submit");
    }

    vkFreeCommandBuffers(device, commandPool, 1, &cmd);
}

void Texture::createImageBarrier(VkCommandPool commandPool, VkQueue queue,
        VkImageLayout newImageLayout) {
    auto cmd = startCommand(commandPool);

    /* create image barrier */
    VkImageSubresourceRange subresourceRange = {};
    subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    subresourceRange.baseMipLevel = 0;
    subresourceRange.levelCount = 1;
    subresourceRange.layerCount = 1;

    VkImageMemoryBarrier imageMemoryBarrier = {};
    imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;

    imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    imageMemoryBarrier.newLayout = newImageLayout;
    imageMemoryBarrier.image = image;
    imageMemoryBarrier.subresourceRange = subresourceRange;
    imageMemoryBarrier.srcAccessMask = 0;

    if (newImageLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
        imageMemoryBarrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT
            | VK_ACCESS_TRANSFER_WRITE_BIT;
        imageMemoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
    }

    /* submit barrier */
    vkCmdPipelineBarrier(cmd, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
            VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
            0,
            0, nullptr,
            0, nullptr,
            1, &imageMemoryBarrier);

    endCommand(cmd, commandPool, queue);
}

void Texture::createSampler() {
    VkSamplerCreateInfo samplerCreateInfo = {};

    samplerCreateInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    samplerCreateInfo.magFilter = VK_FILTER_LINEAR;
    samplerCreateInfo.minFilter = VK_FILTER_LINEAR;
    samplerCreateInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    samplerCreateInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
    samplerCreateInfo.addressModeV = samplerCreateInfo.addressModeU;
    samplerCreateInfo.addressModeW = samplerCreateInfo.addressModeU;
    samplerCreateInfo.mipLodBias = 0.0f;
    samplerCreateInfo.maxAnisotropy = 1;
    samplerCreateInfo.compareOp = VK_COMPARE_OP_NEVER;
    samplerCreateInfo.minLod = 0.0f;
    samplerCreateInfo.maxLod = 0.0f;
    samplerCreateInfo.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;

    if (vkCreateSampler(device, &samplerCreateInfo, nullptr, sampler.replace())
            != VK_SUCCESS) {
        throw std::runtime_error("failed to create sampler");
    }
}

void Texture::createImageView(VkFormat format) {
    VkImageViewCreateInfo viewCreateInfo = {};
    viewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    viewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    viewCreateInfo.format = format;
    viewCreateInfo.components = {
        VK_COMPONENT_SWIZZLE_R,
        VK_COMPONENT_SWIZZLE_G,
        VK_COMPONENT_SWIZZLE_B,
        VK_COMPONENT_SWIZZLE_A
    };

    viewCreateInfo.subresourceRange = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 };
    // TODO Sascha williams has the following things set to layerCount, 1,
    // you had them unitialized (0?). Why?
    viewCreateInfo.subresourceRange.layerCount = 1;
    viewCreateInfo.subresourceRange.levelCount = 1;
    viewCreateInfo.image = image;

    if (vkCreateImageView(device, &viewCreateInfo, nullptr, view.replace())
            != VK_SUCCESS) {
        throw std::runtime_error("failed to create texture image view");
    }

    descriptor.imageLayout = VK_IMAGE_LAYOUT_GENERAL;
    descriptor.imageView = view;
    descriptor.sampler = sampler;
    printf("set descriptor\n");
}

void Texture::setImageLayout(VkCommandBuffer cmdbuffer, VkImage image,
        VkImageAspectFlags aspectMask, VkImageLayout oldImageLayout,
        VkImageLayout newImageLayout, VkImageSubresourceRange subresourceRange,
        VkPipelineStageFlags srcStageMask,
        VkPipelineStageFlags dstStageMask) {

    VkImageMemoryBarrier imageMemoryBarrier = {};
    imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    imageMemoryBarrier.oldLayout = oldImageLayout;
    imageMemoryBarrier.newLayout = newImageLayout;
    imageMemoryBarrier.image = image;
    imageMemoryBarrier.subresourceRange = subresourceRange;

    /* Source access mask controls actions that have to be finished on
     * the old layout before it will be transitioned to the new layout
     */
    switch (oldImageLayout)
    {
        case VK_IMAGE_LAYOUT_UNDEFINED:
            // Image layout is undefined (or does not matter)
            // Only valid as initial layout
            // No flags required, listed only for completeness
            imageMemoryBarrier.srcAccessMask = 0;
            break;

        case VK_IMAGE_LAYOUT_PREINITIALIZED:
            // Image is preinitialized
            // Only valid as initial layout for linear images, preserves memory contents
            // Make sure host writes have been finished
            imageMemoryBarrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT;
            break;

        case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:
            // Image is a color attachment
            // Make sure any writes to the color buffer have been finished
            imageMemoryBarrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
            break;

        case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:
            // Image is a transfer source
            // Make sure any reads from the image have been finished
            imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
            break;

        case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
            // Image is a transfer destination
            // Make sure any writes to the image have been finished
            imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            break;

        case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
            // Image is read by a shader
            // Make sure any shader reads from the image have been finished
            imageMemoryBarrier.srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
            break;
        default:
            printf("fuck\n");
    }

    // Target layouts (new)
    // Destination access mask controls the dependency for the new image layout
    switch (newImageLayout)
    {
        case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
            // Image will be used as a transfer destination
            // Make sure any writes to the image have been finished
            imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            break;

        case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:
            // Image will be used as a transfer source
            // Make sure any reads from and writes to the image have been finished
            imageMemoryBarrier.srcAccessMask = imageMemoryBarrier.srcAccessMask | VK_ACCESS_TRANSFER_READ_BIT;
            imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
            break;

        case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:
            // Image will be used as a color attachment
            // Make sure any writes to the color buffer have been finished
            imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
            imageMemoryBarrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
            break;

        case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL:
            // Image layout will be used as a depth/stencil attachment
            // Make sure any writes to depth/stencil buffer have been finished
            imageMemoryBarrier.dstAccessMask = imageMemoryBarrier.dstAccessMask | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
            break;

        case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
            // Image will be read in a shader (sampler, input attachment)
            // Make sure any writes to the image have been finished
            if (imageMemoryBarrier.srcAccessMask == 0)
            {
                imageMemoryBarrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT | VK_ACCESS_TRANSFER_WRITE_BIT;
            }
            imageMemoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
            break;
        default:
            printf("fuck2\n");
    }

    // Put barrier inside setup command buffer
    vkCmdPipelineBarrier(
            cmdbuffer,
            srcStageMask,
            dstStageMask,
            0,
            0, nullptr,
            0, nullptr,
            1, &imageMemoryBarrier);
}

uint32_t Texture::findMemoryType(VkPhysicalDevice physicalDevice,
        uint32_t typeFilter, VkMemoryPropertyFlags props) {
    VkPhysicalDeviceMemoryProperties memProps;
    vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memProps);

    for (uint32_t i = 0; i < memProps.memoryTypeCount; i++) {
        if (typeFilter & (1 << i)
                && (memProps.memoryTypes[i].propertyFlags & props) == props) {
            return i;
        }
    }

    throw std::runtime_error("failed to find suitable memory");
}


