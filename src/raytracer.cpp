#include "raytracer.h"
#include "scene.h"
#include "pong.h"
#include "testScene.h"
#include "texture.h"

// TODO: Autodetect this at runtime, it has a huge performance gain.
#define GLSL_SHADER

const bool enableValidationLayers = true;

const std::vector<const char *> validationLayers = {
    "VK_LAYER_LUNARG_standard_validation"
};

const std::vector<const char *> deviceExtensions = {
    VK_KHR_SWAPCHAIN_EXTENSION_NAME
    #ifdef GLSL_SHADER
    , VK_NV_GLSL_SHADER_EXTENSION_NAME
    #endif
};

using namespace std;

class RayTracer {
public:
    GLFWwindow *window;
    uint32_t width;
    uint32_t height;

private:
    /* queue indices */
    QueueFamilyIndices queueFamilyIndices;

    /* create connection between raytracer and vulkan */
    VDeleter<VkInstance> instance{vkDestroyInstance};

    /* handle to debug callback */
    VDeleter<VkDebugReportCallbackEXT> callback{instance,
        DestroyDebugReportCallbackEXT};

    /* handle to physical device */
    VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;

    /* handle to logical device */
    VDeleter<VkDevice> device{vkDestroyDevice};

    /* synchronization primitives */
    VDeleter<VkSemaphore> presentSemaphore{device, vkDestroySemaphore};
    VDeleter<VkSemaphore> renderSemaphore{device, vkDestroySemaphore};
    VkSubmitInfo submitInfo;
    VkPipelineStageFlags submitPipelineStages =
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

    /*
     * TODO: resolve weird issue where setup fails if textures
     * is vector<Texture>
     */
    vector<Texture *> textures;

    /* handle to queues, potentials the same */
    VkQueue graphicsQueue;
    VkQueue presentQueue;
    VkQueue computeQueue;

    /* handle to surface to present rendered images to */
    VDeleter<VkSurfaceKHR> surface{instance, vkDestroySurfaceKHR};

    /* handle to command pool */
    VDeleter<VkCommandPool> commandPool{device, vkDestroyCommandPool};

    /* handle to swapchain and its meta data */
    VDeleter<VkSwapchainKHR> swapChain{device, vkDestroySwapchainKHR};
    std::vector<VkImage> swapChainImages;
    VkFormat swapChainImageFormat;
    VkExtent2D swapChainExtent;
    uint32_t currentBuffer = 0;

    VDeleter<VkDescriptorPool> descriptorPool{device, vkDestroyDescriptorPool};

    /* images */
    std::vector<VDeleter<VkImageView>> swapChainImageViews;

    /* command buffer */
    std::vector<VkCommandBuffer> commandBuffers;

    /* framebuffer */
    std::vector<VDeleter<VkFramebuffer>> swapChainFrameBuffers;

    VDeleter<VkRenderPass> renderPass{device, vkDestroyRenderPass};

    /* pipeline cache */
    VDeleter<VkPipelineCache> pipelineCache{device, vkDestroyPipelineCache};

    /* compute */
    VDeleter<VkCommandPool> computeCommandPool{device, vkDestroyCommandPool};
    VDeleter<VkFence> computeFence{device, vkDestroyFence};
    VDeleter<VkDescriptorSetLayout> computeDescriptorSetLayout{device,
        vkDestroyDescriptorSetLayout};
    VDeleter<VkPipelineLayout> computePipelineLayout{device,
        vkDestroyPipelineLayout};
    VDeleter<VkPipeline> computePipeline{device, vkDestroyPipeline};
    VkCommandBuffer computeCommandBuffer;
    VkDescriptorSet computeDescriptorSet;

    /* graphics */
    VDeleter<VkDescriptorSetLayout> graphicsDescriptorSetLayout{device,
        vkDestroyDescriptorSetLayout};
    VkDescriptorSet graphicsDescriptorSetPreCompute;
    VkDescriptorSet graphicsDescriptorSet;
    VDeleter<VkPipelineLayout> graphicsPipelineLayout{device,
        vkDestroyPipelineLayout};
    VDeleter<VkPipeline> graphicsPipeline{device, vkDestroyPipeline};

    VDeleter<VkPipelineLayout> pipelineLayout{device, vkDestroyPipelineLayout};

    /* sphere buffer */
    Buffer objectBuffer;

public:
    std::vector<Object> objects{MAX_OBJECTS};

    /* camera buffer */
    Buffer cameraBuffer;
    struct rayCamera camera;

    RayTracer(int width, int height) : width(width), height(height) {
        initWindow();
        memset((void *) objects.data(), 0, sizeof(Object) * objects.size());
    }

    ~RayTracer() {
        vkDeviceWaitIdle(device);

        for( auto tex: textures ) {
            delete tex;
        }

        glfwDestroyWindow(window);
        glfwTerminate();
    }

    void initVulkan(vector<pair<uint32_t, const char *>> textureFiles) {
        createInstance();
        setupDebugCallback();
        createSurface();

        pickPhysicalDevice();
        createLogicalDevice();

        createSemaphores();
        createCommandPool();
        createSwapChain();
        createImageViews();

        createRenderPass();
        createPipelineCache();
        createFrameBuffer();

        createUniformBuffers();
        createTextures(textureFiles);
        createDescriptorSetLayout();
        createGraphicsPipeline();
        createDescriptorPool();
        createDescriptorSet();
        createCompute();
        createComputeCommandBuffers();
        createCommandBuffers();
    }


private:
    void initWindow() {
        glfwInit();

        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

        window = glfwCreateWindow(width, height, "Raytracer", nullptr, nullptr);
    }

    void createInstance() {
        if (enableValidationLayers && !checkValidationLayerSupport()) {
            throw std::runtime_error("validation layers requested, "
                    "but not available");
        }

        VkInstanceCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;

        /* find global extensions supported by GLFW */
        createInfo.ppEnabledExtensionNames =
            glfwGetRequiredInstanceExtensions(&createInfo.enabledExtensionCount);

        /* attach extensions to instance */
        auto extensions = getRequiredExtensions();
        createInfo.enabledExtensionCount = extensions.size();
        createInfo.ppEnabledExtensionNames = extensions.data();

        /* attach validation layers to instance */
        if (enableValidationLayers) {
            createInfo.enabledLayerCount = validationLayers.size();
            createInfo.ppEnabledLayerNames = validationLayers.data();
        } else {
            createInfo.enabledLayerCount = 0;
        }

        if (vkCreateInstance(&createInfo, nullptr, instance.replace()) !=
                VK_SUCCESS) {
            throw std::runtime_error("failed to create instance");
        }
    }

    /* ensure all requested validation layers exist */
    bool checkValidationLayerSupport() {
        uint32_t layerCount;
        vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

        /* generate list of all suported layers */
        std::vector<VkLayerProperties> availableLayers(layerCount);
        vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

        /* check that each requested validation layer exists */
        for (auto layerName : validationLayers) {
            bool layerFound = false;

            for (const auto &layerProperties : availableLayers) {
                if (!strcmp(layerName, layerProperties.layerName)) {
                    layerFound = true;
                    break;
                }
            }

            if (!layerFound) return false;
        }

        return true;
    }

    /* return list of required extensions */
    std::vector<const char *> getRequiredExtensions() {
        std::vector<const char *> extensions;

        uint32_t glfwExtensionCount = 0;
        auto glfwExtensions = glfwGetRequiredInstanceExtensions(
                &glfwExtensionCount);

        for (auto i = 0U; i < glfwExtensionCount; i++) {
            extensions.push_back(glfwExtensions[i]);
        }

        if (enableValidationLayers) {
            extensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
        }

        return extensions;
    }

    void setupDebugCallback() {
        if (!enableValidationLayers) return;

        VkDebugReportCallbackCreateInfoEXT createInfo = {};
        createInfo.sType =
            VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;

        /* handle all warnings and errors */
        createInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT
            | VK_DEBUG_REPORT_WARNING_BIT_EXT;

        createInfo.pfnCallback = debugCallback;

        if (CreateDebugReportCallbackEXT(instance, &createInfo, nullptr,
                    callback.replace()) != VK_SUCCESS) {
            throw std::runtime_error("failed to setup debug callback");
        }
    }

    void pickPhysicalDevice() {
        uint32_t deviceCount = 0;
        vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr);

        if (deviceCount == 0) {
            throw std::runtime_error("failed to find GPUs with Vulkan support");
        }

        std::vector<VkPhysicalDevice> devices(deviceCount);
        vkEnumeratePhysicalDevices(instance, &deviceCount, devices.data());

        for (const auto &device : devices) {
            if (isDeviceSuitable(device)) {
                physicalDevice = device;
                break;
            }
        }

        if (physicalDevice == VK_NULL_HANDLE) {
            throw std::runtime_error("failed to find a suitable GPU");
        }
    }

    bool isDeviceSuitable(VkPhysicalDevice device) {
        VkPhysicalDeviceProperties deviceProperties;
        vkGetPhysicalDeviceProperties(device, &deviceProperties);

        VkPhysicalDeviceFeatures deviceFeatures;
        vkGetPhysicalDeviceFeatures(device, &deviceFeatures);

        /* specify any attributes required of the physical device */

        QueueFamilyIndices indices = findQueueFamilies(device);
        bool extensionsSupported = checkDeviceExtensionSupport(device);
        bool swapChainAdequate = false;

        if (extensionsSupported) {
            auto swapChainSupport = querySwapChainSupport(device);
            swapChainAdequate = swapChainSupport.isAdequate();
        }

        return indices.isComplete() && extensionsSupported && swapChainAdequate;
    }

    /* whether physical device supports specific command queue fmailies */
    QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device) {
        QueueFamilyIndices indices;

        uint32_t queueFamilyCount = 0;
        vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount,
                nullptr);

        std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
        vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount,
                queueFamilies.data());

        /* preload present support */
        std::vector<VkBool32> presentSupport(queueFamilyCount);
        for (auto i = 0U; i < queueFamilyCount; i++) {
            vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface,
                    &presentSupport[i]);
        }

        auto i = 0;
        /* look for seperate compute queue */
        for (const auto &queueFamily : queueFamilies) {
            if (queueFamily.queueFlags & VK_QUEUE_COMPUTE_BIT
                    && !(queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT)) {
                indices.compute = i;
                break;
            }
            i++;
        }

        i = 0;
        /* look for shared present and graphics queue */
        for (const auto &queueFamily : queueFamilies) {
            if (queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
                /* prefer lowest index queue */
                if (indices.graphics == -1) {
                    indices.graphics = i;
                }

                if (presentSupport[i]) {
                    indices.graphics = i;
                    indices.present = i;
                    break;
                }
            }
            i++;
        }

        /* failed to find optimal queue configurations*/
        if (!indices.isComplete()) {
            i = 0;
            for (const auto &queueFamily : queueFamilies) {
                if (indices.compute == -1
                        && queueFamily.queueFlags & VK_QUEUE_COMPUTE_BIT) {
                    indices.compute = i;
                }

                if (indices.present == -1 && presentSupport[i]) {
                    indices.present = i;
                }
                i++;
            }
        }

        if (!indices.isComplete()) {
            throw std::runtime_error("failed to find queues");
        }

        return indices;
    }

    bool checkDeviceExtensionSupport(VkPhysicalDevice device) {
        uint32_t extensionCount;
        vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount,
                nullptr);

        std::vector<VkExtensionProperties> availableExtensions(extensionCount);
        vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount,
                availableExtensions.data());

        std::set<std::string> requiredExtensions(deviceExtensions.begin(),
                deviceExtensions.end());

        /* remove each available extension from set of required extensions */
        for (const auto &extension : availableExtensions) {
            requiredExtensions.erase(extension.extensionName);
        }

        return requiredExtensions.empty();
    }

    SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device) {
        SwapChainSupportDetails details;

        /*
         * get surface capabilities (min/max number of images in swapchain,
         * min/max width and height of images)
         */
        vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface,
                &details.capabilities);

        /* get surface formats (pixel format, color space) */
        uint32_t formatCount;
        vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount,
                nullptr);

        if (formatCount != 0) {
            details.formats.resize(formatCount);
            vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount,
                    details.formats.data());
        }

        /* get available presentation modes */
        uint32_t presentModeCount;
        vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface,
                &presentModeCount, nullptr);

        if (presentModeCount != 0) {
            details.presentModes.resize(presentModeCount);
            vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface,
                    &presentModeCount, details.presentModes.data());
        }

        return details;
    }

    void createLogicalDevice() {
        VkDeviceCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;

        queueFamilyIndices = findQueueFamilies(physicalDevice);

        /*
         * remove duplicates in case we didn't find seperate compute and
         * graphics queues
         */
        std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
        std::set<int> uniqueQueueFamilies = {
            queueFamilyIndices.graphics,
            queueFamilyIndices.present,
            queueFamilyIndices.compute
        };

        /* initialize shared queueCreateInfo */
        float queuePriority = 1.0f;

        VkDeviceQueueCreateInfo queueCreateInfo = {};
        queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queueCreateInfo.queueCount = 1;
        queueCreateInfo.pQueuePriorities = &queuePriority;

        /* create all unique queues */
        for (auto queueFamily : uniqueQueueFamilies) {
            queueCreateInfo.queueFamilyIndex = queueFamily;
            queueCreateInfos.push_back(queueCreateInfo);
        }

        /* attach queues */
        createInfo.pQueueCreateInfos = queueCreateInfos.data();
        createInfo.queueCreateInfoCount = (uint32_t) queueCreateInfos.size();

        /* attach features */
        VkPhysicalDeviceFeatures deviceFeatures = {};
        createInfo.pEnabledFeatures = &deviceFeatures;

        /* attach extensions */
        createInfo.enabledExtensionCount = deviceExtensions.size();
        createInfo.ppEnabledExtensionNames = deviceExtensions.data();

        /* TODO validation layers on logical devices have been depricated */
        if (enableValidationLayers) {
            createInfo.enabledLayerCount = validationLayers.size();
            createInfo.ppEnabledLayerNames = validationLayers.data();
        } else {
            createInfo.enabledLayerCount = 0;
        }

        if (vkCreateDevice(physicalDevice, &createInfo, nullptr,
                    device.replace()) != VK_SUCCESS) {
            throw std::runtime_error("failed to create logical device");
        }

        /* get handles to newly created queues */
        vkGetDeviceQueue(device, queueFamilyIndices.graphics, 0, &graphicsQueue);
        vkGetDeviceQueue(device, queueFamilyIndices.present, 0, &presentQueue);
        vkGetDeviceQueue(device, queueFamilyIndices.compute, 0, &computeQueue);
    }

    void createSurface() {
        if (glfwCreateWindowSurface(instance, window, nullptr,
                    surface.replace()) != VK_SUCCESS) {
            throw std::runtime_error("failed to create window surface");
        }
    }

    void createSemaphores() {
        VkSemaphoreCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

        if (vkCreateSemaphore(device, &createInfo, nullptr,
                    presentSemaphore.replace()) != VK_SUCCESS
                || vkCreateSemaphore(device, &createInfo, nullptr,
                    renderSemaphore.replace()) != VK_SUCCESS) {
            throw std::runtime_error("failed to create semaphores");
        }

        submitInfo = {};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.pWaitDstStageMask = &submitPipelineStages;
        submitInfo.waitSemaphoreCount = 1;
        submitInfo.pWaitSemaphores = &presentSemaphore;
        submitInfo.signalSemaphoreCount = 1;
        submitInfo.pSignalSemaphores = &renderSemaphore;
    }

    void createCommandPool() {
        VkCommandPoolCreateInfo poolInfo = {};
        poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        poolInfo.queueFamilyIndex = queueFamilyIndices.graphics;

        if (vkCreateCommandPool(device, &poolInfo, nullptr,
                    commandPool.replace()) != VK_SUCCESS) {
            throw std::runtime_error("failed to create graphics command pool!");
        }
    }
    void createSwapChain() {
        auto swapChainSupport = querySwapChainSupport(physicalDevice);

        auto surfaceFormat = chooseSwapSurfaceFormat(swapChainSupport.formats);
        auto presentMode = chooseSwapPresentMode(swapChainSupport.presentModes);
        auto extent = chooseSwapExtent(swapChainSupport.capabilities);

        /* set length of swap chain */
        uint32_t imageCount = swapChainSupport.capabilities.minImageCount + 1;
        if (swapChainSupport.capabilities.maxImageCount > 0
                && imageCount > swapChainSupport.capabilities.maxImageCount) {
            imageCount = swapChainSupport.capabilities.maxImageCount;
        }

        VkSwapchainCreateInfoKHR createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
        createInfo.surface = surface;

        createInfo.minImageCount = imageCount;
        createInfo.imageFormat = surfaceFormat.format;
        createInfo.imageColorSpace = surfaceFormat.colorSpace;
        createInfo.imageExtent = extent;
        createInfo.imageArrayLayers = 1;
        createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

        auto indices = findQueueFamilies(physicalDevice);
        uint32_t queueFamilyIndices[] = {
                (uint32_t) indices.graphics,
                (uint32_t) indices.present
        };

        /* handle case of two different queues */
        if (queueFamilyIndices[0] != queueFamilyIndices[1]) {
            createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
            createInfo.queueFamilyIndexCount = 2;
            createInfo.pQueueFamilyIndices = queueFamilyIndices;
        } else {
            createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        }

        /* specify no transforms need to be applied */
        createInfo.preTransform = swapChainSupport.capabilities.currentTransform;
        /* ignore alpha channel */
        createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
        /* don't draw obscured pixels */
        createInfo.clipped = VK_TRUE;

        createInfo.presentMode = presentMode;
        createInfo.oldSwapchain = VK_NULL_HANDLE;

        if (vkCreateSwapchainKHR(device, &createInfo, nullptr,
                    swapChain.replace()) != VK_SUCCESS) {
            throw std::runtime_error("failed to crate swap chain");
        }

        /* get swap chain images */
        vkGetSwapchainImagesKHR(device, swapChain, &imageCount, nullptr);
        swapChainImages.resize(imageCount);
        vkGetSwapchainImagesKHR(device, swapChain, &imageCount,
                swapChainImages.data());

        /* keep references to format and extent */
        swapChainImageFormat = surfaceFormat.format;
        swapChainExtent = extent;
    }

    VkSurfaceFormatKHR chooseSwapSurfaceFormat(
            const std::vector<VkSurfaceFormatKHR> &availableFormats) {
        /* surface has no preffered format, so we can specify */
        if (availableFormats.size() == 1
                && availableFormats[0].format == VK_FORMAT_UNDEFINED) {
            return { VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
        }

        /* try to find preffered format */
        for (const auto &availableFormat : availableFormats) {
            if (availableFormat.format == VK_FORMAT_B8G8R8A8_UNORM &&
                    availableFormat.colorSpace ==
                    VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
                return availableFormat;
            }
        }

        return availableFormats[0];
    }

    VkPresentModeKHR chooseSwapPresentMode(
            const std::vector<VkPresentModeKHR> availablePresentModes) {
        auto bestMode = VK_PRESENT_MODE_FIFO_KHR;

        for (const auto &availablePresentMode : availablePresentModes) {
            if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR) {
                return availablePresentMode;
            } else if (availablePresentMode == VK_PRESENT_MODE_IMMEDIATE_KHR) {
                bestMode = availablePresentMode;
            }
        }

        return bestMode;
    }

    VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR &capabilities) {
        /* forced to match resolution of the extent */
        if (capabilities.currentExtent.width !=
                std::numeric_limits<uint32_t>::max()) {
            return capabilities.currentExtent;
        } else {
            VkExtent2D actualExtent = { width, height };

            /* clamp width and height between supported limits */
            actualExtent.width = std::max(
                    capabilities.minImageExtent.width,
                    std::min(
                        capabilities.maxImageExtent.width,
                        actualExtent.width
                    )
            );

            actualExtent.height = std::max(
                    capabilities.minImageExtent.height,
                    std::min(
                        capabilities.maxImageExtent.height,
                        actualExtent.height
                    )
            );

            return actualExtent;
        }
    }

    void createImageViews() {
        swapChainImageViews.resize(swapChainImages.size(),
                VDeleter<VkImageView>{device, vkDestroyImageView});

        for (auto i = 0U; i < swapChainImages.size(); i++) {
            VkImageViewCreateInfo createInfo = {};
            createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
            createInfo.image = swapChainImages[i];

            createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
            createInfo.format = swapChainImageFormat;

            createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
            createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
            createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
            createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;

            createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            createInfo.subresourceRange.baseMipLevel = 0;
            createInfo.subresourceRange.levelCount = 1;
            createInfo.subresourceRange.baseArrayLayer = 0;
            createInfo.subresourceRange.layerCount = 1;

            if (vkCreateImageView(device, &createInfo, nullptr,
                        swapChainImageViews[i].replace()) != VK_SUCCESS) {
                throw std::runtime_error("failed to create image view");
            }
        }
    }

    void createRenderPass() {
        VkAttachmentDescription colorAttachment = {};
        colorAttachment.format = swapChainImageFormat;
        colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
        colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;


        /* subpass */
        VkSubpassDescription subpassDescription = {};
        subpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;

        /* color attachment */
        VkAttachmentReference colorAttachmentRef = {};
        colorAttachmentRef.attachment = 0;
        colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        subpassDescription.colorAttachmentCount = 1;
        subpassDescription.pColorAttachments = &colorAttachmentRef;

        /* dependencies for layout transition */
        std::array<VkSubpassDependency, 2> dependencies;

        dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
        dependencies[0].dstSubpass = 0;
        dependencies[0].srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
        dependencies[0].dstStageMask =
            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependencies[0].srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
        dependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT
            | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        dependencies[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

        dependencies[1].srcSubpass = 0;
        dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
        dependencies[1].srcStageMask =
            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependencies[1].dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
        dependencies[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        dependencies[1].dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
        dependencies[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

        VkRenderPassCreateInfo renderPassInfo = {};
        renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
        renderPassInfo.attachmentCount = (uint32_t) 1;
        renderPassInfo.pAttachments = &colorAttachment;
        renderPassInfo.subpassCount = 1;
        renderPassInfo.pSubpasses = &subpassDescription;
        renderPassInfo.dependencyCount = (uint32_t) dependencies.size();
        renderPassInfo.pDependencies = dependencies.data();

        if (vkCreateRenderPass(device, &renderPassInfo, nullptr,
                    renderPass.replace()) != VK_SUCCESS) {
            throw std::runtime_error("failed to create render pass");
        }
    }

    void createPipelineCache() {
        VkPipelineCacheCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;

        if (vkCreatePipelineCache(device, &createInfo, nullptr,
                    pipelineCache.replace()) != VK_SUCCESS) {
            throw std::runtime_error("failed to created pipeline cache");
        }
    }

    void createFrameBuffer() {
        VkImageView attachments[1];
        swapChainFrameBuffers.resize(swapChainImageViews.size(),
                VDeleter<VkFramebuffer> {device, vkDestroyFramebuffer});

        //TODO add depth/stencil attachment

        VkFramebufferCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        createInfo.renderPass = renderPass;
        createInfo.attachmentCount = 1;
        createInfo.pAttachments = attachments;
        createInfo.width = swapChainExtent.width;
        createInfo.height = swapChainExtent.height;
        createInfo.layers = 1;

        /* create frame buffer for every swap chain image */
        for (uint32_t i = 0; i < swapChainImageViews.size(); i++) {
            attachments[0] = swapChainImageViews[i];

            if (vkCreateFramebuffer(device, &createInfo, nullptr,
                        swapChainFrameBuffers[i].replace()) != VK_SUCCESS) {
                throw std::runtime_error("failed to create frame buffer");
            }
        }
    }

    void createUniformBuffers() {
        VkDeviceSize bufferSize = sizeof(rayCamera);
        cameraBuffer.buffer = VDeleter<VkBuffer>{device, vkDestroyBuffer};
        cameraBuffer.memory = VDeleter<VkDeviceMemory>{device, vkFreeMemory};

        createBuffer(VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
                | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                &cameraBuffer, bufferSize);

        /* initialize so deallocation is handle cleanly */
        bufferSize = MAX_OBJECTS * sizeof(Object);
        objectBuffer.buffer = VDeleter<VkBuffer>{device, vkDestroyBuffer};
        objectBuffer.memory = VDeleter<VkDeviceMemory>{device, vkFreeMemory};

        createBuffer(VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
                | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                &objectBuffer, bufferSize);
    }

    VkResult createBuffer(VkBufferUsageFlags usageFlags,
            VkMemoryPropertyFlags memoryPropertyFlags,
            Buffer *buffer, VkDeviceSize size) {
        /* crate buffer handle */
        VkBufferCreateInfo bufferCreateInfo = {};
        bufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        bufferCreateInfo.usage = usageFlags;
        bufferCreateInfo.size = size;

        if (vkCreateBuffer(device, &bufferCreateInfo, nullptr,
                    buffer->buffer.replace()) != VK_SUCCESS) {
            throw std::runtime_error("failed to create buffer");
        }

        /* allocate memory for the buffer */
        VkMemoryAllocateInfo memAlloc =  {};
        memAlloc.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;

        VkMemoryRequirements memReqs;
        vkGetBufferMemoryRequirements(device, buffer->buffer, &memReqs);
        memAlloc.allocationSize = memReqs.size;
        memAlloc.memoryTypeIndex = findMemoryType(memReqs.memoryTypeBits,
                memoryPropertyFlags);

        if (vkAllocateMemory(device, &memAlloc, nullptr,
                    buffer->memory.replace()) != VK_SUCCESS) {
            throw std::runtime_error("failed to allocate buffer memory");
        }

        buffer->alignment = memReqs.alignment;
        buffer->size = memAlloc.allocationSize;
        buffer->usageFlags = usageFlags;
        buffer->memoryPropertyFlags = memoryPropertyFlags;

        // Initialize a default descriptor that covers the whole buffer size
        buffer->descriptor.offset = 0;
        buffer->descriptor.buffer = buffer->buffer;
        buffer->descriptor.range = VK_WHOLE_SIZE;

        // Attach the memory to the buffer object
        return vkBindBufferMemory(device, buffer->buffer, buffer->memory, 0);
    }

    uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags props) {
        VkPhysicalDeviceMemoryProperties memProps;
        vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memProps);

        for (uint32_t i = 0; i < memProps.memoryTypeCount; i++) {
            if (typeFilter & (1 << i)
                    && (memProps.memoryTypes[i].propertyFlags & props) == props) {
                return i;
            }
        }

        throw std::runtime_error("failed to find suitable memory");
    }

    void updateUniformBuffer(Buffer *buffer, void *data, VkDeviceSize size) {
       /* map buffer into cpu space */
        if (vkMapMemory(device, buffer->memory, 0, VK_WHOLE_SIZE, 0 ,
                    &buffer->mapped) != VK_SUCCESS) {
            throw std::runtime_error("failed to map memory in");
        }
        /* update buffer */
        memcpy(buffer->mapped, data, size);
        /* remove buffer from cpu space */
        vkUnmapMemory(device, buffer->memory);
    }

    void createTextures(vector<pair<uint32_t, const char *>> textureFiles) {
        textures.emplace_back(new Texture(device, physicalDevice, commandPool,
                graphicsQueue, width * ANTI_ALIASING, height * ANTI_ALIASING, 0,
                VK_FORMAT_R8G8B8A8_UNORM));
        for (auto &p : textureFiles) {
            textures.emplace_back(new Texture(device, physicalDevice,
                        commandPool, graphicsQueue, p.second, p.first,
                        VK_FORMAT_R8G8B8A8_UNORM));
        }
    }

    void createDescriptorSetLayout() {
        /* bind descriptor for fragment shader image sampler */
        VkDescriptorSetLayoutBinding setLayout = {};
        setLayout.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        setLayout.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
        setLayout.binding = 0;
        setLayout.descriptorCount = 1;

        VkDescriptorSetLayoutCreateInfo descriptorLayout = {};
        descriptorLayout.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
        descriptorLayout.pBindings = &setLayout;
        descriptorLayout.bindingCount = 1;

        if (vkCreateDescriptorSetLayout(device, &descriptorLayout, nullptr,
                    graphicsDescriptorSetLayout.replace())!= VK_SUCCESS) {
            throw std::runtime_error("failed to create descriptor set");
        }

        VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo {};
        pipelineLayoutCreateInfo.sType =
            VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        pipelineLayoutCreateInfo.setLayoutCount = 1;
        pipelineLayoutCreateInfo.pSetLayouts = &graphicsDescriptorSetLayout;

        if (vkCreatePipelineLayout(device, &pipelineLayoutCreateInfo, nullptr,
                    graphicsPipelineLayout.replace()) != VK_SUCCESS) {
            throw std::runtime_error("failed to create graphics pipeline layout");
        }
    }

    void createGraphicsPipeline() {
        /* how how geometry should be drawn */
        VkPipelineInputAssemblyStateCreateInfo inputAssemblyState = {};
        inputAssemblyState.sType =
            VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
        inputAssemblyState.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
        inputAssemblyState.primitiveRestartEnable = VK_FALSE;

        /* rasterization */
        VkPipelineRasterizationStateCreateInfo rasterizationState = {};
        rasterizationState.sType =
            VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
        rasterizationState.polygonMode = VK_POLYGON_MODE_FILL;
        rasterizationState.cullMode = VK_CULL_MODE_FRONT_BIT;
        rasterizationState.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
        rasterizationState.depthClampEnable = VK_FALSE;
        rasterizationState.lineWidth = 1.0f;

        /* color blend attachment */
        VkPipelineColorBlendAttachmentState blendAttachmentState = {};
        blendAttachmentState.colorWriteMask = 0xf;
        blendAttachmentState.blendEnable = VK_FALSE;

        /* color blend state */
        VkPipelineColorBlendStateCreateInfo colorBlendState = {};
        colorBlendState.sType =
            VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
        colorBlendState.attachmentCount = 1;
        colorBlendState.pAttachments = &blendAttachmentState;

        /* depth and stencil tests */
        /* VkPipelineDepthStencilStateCreateInfo depthStencilState = {}; */
        /* depthStencilState.sType = */
        /*     VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO; */
        /* depthStencilState.depthTestEnable = VK_FALSE; */
        /* depthStencilState.depthWriteEnable = VK_FALSE; */
        /* depthStencilState.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL; */
        /* depthStencilState.front = depthStencilState.back; */
        /* depthStencilState.back.compareOp = VK_COMPARE_OP_ALWAYS; */

        /* viewport state */
        VkPipelineViewportStateCreateInfo viewportState = {};
        viewportState.sType =
            VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
        viewportState.viewportCount = 1;
        viewportState.scissorCount = 1;
        viewportState.flags = 0;

        /* multisample state */
        VkPipelineMultisampleStateCreateInfo multisampleState = {};
        multisampleState.sType =
            VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
        multisampleState.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
        multisampleState.flags = 0;

        /* dynamic state */
        std::vector<VkDynamicState> dynamicStateEnables = {
            VK_DYNAMIC_STATE_VIEWPORT,
            VK_DYNAMIC_STATE_SCISSOR
        };

        VkPipelineDynamicStateCreateInfo dynamicState = {};
        dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
        dynamicState.pDynamicStates = dynamicStateEnables.data();
        dynamicState.dynamicStateCount = dynamicStateEnables.size();
        dynamicState.flags = 0;

        /* display pipeline */
        VDeleter<VkShaderModule> vertShaderModule{device, vkDestroyShaderModule};
        VDeleter<VkShaderModule> fragShaderModule{device, vkDestroyShaderModule};

        std::array<VkPipelineShaderStageCreateInfo, 2> shaderStages;
#ifdef GLSL_SHADER
        shaderStages[0] = loadShader("../shaders/shader.vert",
                VK_SHADER_STAGE_VERTEX_BIT, vertShaderModule);
        shaderStages[1] = loadShader("../shaders/shader.frag",
                VK_SHADER_STAGE_FRAGMENT_BIT, fragShaderModule);
#else
        shaderStages[0] = loadShader("../shaders/vert.spv",
                VK_SHADER_STAGE_VERTEX_BIT, vertShaderModule);
        shaderStages[1] = loadShader("../shaders/frag.spv",
                VK_SHADER_STAGE_FRAGMENT_BIT, fragShaderModule);
#endif

        /* vertex input */
        VkPipelineVertexInputStateCreateInfo emptyInputState{};
        emptyInputState.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
        emptyInputState.vertexAttributeDescriptionCount = 0;
        emptyInputState.pVertexAttributeDescriptions = nullptr;
        emptyInputState.vertexBindingDescriptionCount = 0;
        emptyInputState.pVertexBindingDescriptions = nullptr;

        /* create pipeline */
        VkGraphicsPipelineCreateInfo pipelineCreateInfo = {};
        pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        pipelineCreateInfo.layout = graphicsPipelineLayout;
        pipelineCreateInfo.renderPass = renderPass;
        pipelineCreateInfo.flags = 0;
        pipelineCreateInfo.basePipelineIndex = -1;
        pipelineCreateInfo.basePipelineHandle = VK_NULL_HANDLE;

        pipelineCreateInfo.pVertexInputState = &emptyInputState;

        pipelineCreateInfo.pInputAssemblyState = &inputAssemblyState;
        pipelineCreateInfo.pRasterizationState = &rasterizationState;
        pipelineCreateInfo.pColorBlendState = &colorBlendState;
        pipelineCreateInfo.pMultisampleState = &multisampleState;
        pipelineCreateInfo.pViewportState = &viewportState;
        /* TODO pipelineCreateInfo.pDepthStencilState = &depthStencilState; */
        pipelineCreateInfo.pDynamicState = &dynamicState;
        pipelineCreateInfo.stageCount = shaderStages.size();
        pipelineCreateInfo.pStages = shaderStages.data();
        pipelineCreateInfo.renderPass = renderPass;

        if (vkCreateGraphicsPipelines(device, pipelineCache, 1,
                    &pipelineCreateInfo, nullptr, graphicsPipeline.replace())
                != VK_SUCCESS) {
            throw std::runtime_error("failed to create pipeline");
        }
    }

    VkPipelineShaderStageCreateInfo loadShader(const char *fileName,
            VkShaderStageFlagBits stage, VDeleter<VkShaderModule> &module) {
        /* create shader module create info */
        std::ifstream is(fileName,
                std::ios::binary | std::ios::in | std::ios::ate);

        if (!is.is_open()) {
            throw std::runtime_error("failed to open shader file");
        }

        size_t size = is.tellg();
        is.seekg(0, std::ios::beg);

        std::vector<char> shaderCode(size);
        is.read(shaderCode.data(), size);
        is.close();


        VkShaderModuleCreateInfo moduleCreateInfo{};
        moduleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        moduleCreateInfo.codeSize = size;
        moduleCreateInfo.pCode = (uint32_t*)shaderCode.data();

        if (vkCreateShaderModule(device, &moduleCreateInfo, nullptr,
                    module.replace()) != VK_SUCCESS) {
            throw std::runtime_error("failed to load shader");
        }

        VkPipelineShaderStageCreateInfo shaderStage = {};
        shaderStage.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        shaderStage.stage = stage;
        shaderStage.module = module;
        shaderStage.pName = "main";

        return shaderStage;
    }

    void createDescriptorPool() {
        std::vector<VkDescriptorPoolSize> poolSizes = {
            { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 2 },
            { VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 4 },
            { VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1 }
        };

        VkDescriptorPoolCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
        createInfo.poolSizeCount = poolSizes.size();
        createInfo.pPoolSizes = poolSizes.data();
        createInfo.maxSets = 3;

        if (vkCreateDescriptorPool(device, &createInfo, nullptr,
                    descriptorPool.replace()) != VK_SUCCESS) {
            throw std::runtime_error("failed to create descriptor pool");
        }
    }

    void createDescriptorSet() {
        VkDescriptorSetAllocateInfo allocInfo = {};
        allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
        allocInfo.descriptorPool = descriptorPool;
        allocInfo.descriptorSetCount = 1;
        allocInfo.pSetLayouts = &graphicsDescriptorSetLayout;

        if (vkAllocateDescriptorSets(device, &allocInfo,
                    &graphicsDescriptorSet) != VK_SUCCESS) {
            throw std::runtime_error("failed to allocate descriptor set");
        }

        VkWriteDescriptorSet descriptorWrite = {};
        descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrite.dstSet = graphicsDescriptorSet;
        descriptorWrite.descriptorType =
            VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        descriptorWrite.dstBinding = 0;
        descriptorWrite.pImageInfo = &textures[0]->descriptor;
        descriptorWrite.descriptorCount = 1;

        vkUpdateDescriptorSets(device, 1, &descriptorWrite, 0, nullptr);
    }

    void createCompute() {
        /* specifiy inputs and outputs of compute */
        std::vector<VkDescriptorSetLayoutBinding> setLayoutBindings {
            {
                1, //binding
                VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, //descriptorType
                1, //descriptorCount
                VK_SHADER_STAGE_COMPUTE_BIT //stageFlag
            }, {
                2,
                VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                1,
                VK_SHADER_STAGE_COMPUTE_BIT
            }
        };

        for (auto texture : textures) {
            setLayoutBindings.push_back(texture->getDescriptorSetLayoutBinding());
        }

        /* create descriptor layout */
        VkDescriptorSetLayoutCreateInfo descriptorLayout = {};
        descriptorLayout.sType =
            VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
        descriptorLayout.bindingCount = setLayoutBindings.size();
        descriptorLayout.pBindings = setLayoutBindings.data();

        if (vkCreateDescriptorSetLayout(device, &descriptorLayout, nullptr,
                    computeDescriptorSetLayout.replace()) != VK_SUCCESS) {
            throw std::runtime_error("failed to create compute descriptor "
                    "layout");
        }

        /* create pipeline layout */
        VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo = {};
        pipelineLayoutCreateInfo.sType =
            VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        pipelineLayoutCreateInfo.setLayoutCount = 1;
        pipelineLayoutCreateInfo.pSetLayouts = &computeDescriptorSetLayout;

        if (vkCreatePipelineLayout(device, &pipelineLayoutCreateInfo, nullptr,
                   computePipelineLayout.replace()) != VK_SUCCESS) {
            throw std::runtime_error("failed to create compute pipeline "
                    "layou");
        }

        /* allocate descriptor set */
        VkDescriptorSetAllocateInfo allocInfo = {};
        allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
        allocInfo.descriptorPool = descriptorPool;
        allocInfo.pSetLayouts = &computeDescriptorSetLayout;
        allocInfo.descriptorSetCount = 1;

        if (vkAllocateDescriptorSets(device, &allocInfo, &computeDescriptorSet)
                != VK_SUCCESS) {
            throw std::runtime_error("failed to alloc compute descriptor set");
        }

        /* populate descriptor set */
        std::vector<VkWriteDescriptorSet> computeWriteDescriptorSet {
            writeDescriptorSet(
                    computeDescriptorSet,
                    VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                    1,
                    &cameraBuffer.descriptor),
                writeDescriptorSet(
                    computeDescriptorSet,
                    VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                    2,
                    &objectBuffer.descriptor)
        };

        for (auto &texture : textures) {
            computeWriteDescriptorSet.push_back(texture->getWriteDescriptorSet(
                        computeDescriptorSet));
        }

        vkUpdateDescriptorSets(device, computeWriteDescriptorSet.size(),
                computeWriteDescriptorSet.data(), 0, nullptr);

        /* create pipeline */
        VDeleter<VkShaderModule> computeShaderModule{device,
            vkDestroyShaderModule};

        VkComputePipelineCreateInfo computePipelineCreateInfo = {};
        computePipelineCreateInfo.sType =
            VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
        computePipelineCreateInfo.layout = computePipelineLayout;
        computePipelineCreateInfo.flags = 0;
        #ifdef GLSL_SHADER
        computePipelineCreateInfo.stage = loadShader("../shaders/shader.comp",
                VK_SHADER_STAGE_COMPUTE_BIT, computeShaderModule);
        #else
        computePipelineCreateInfo.stage = loadShader("../shaders/comp.spv",
                VK_SHADER_STAGE_COMPUTE_BIT, computeShaderModule);
        #endif

        if (vkCreateComputePipelines(device, pipelineCache, 1,
                    &computePipelineCreateInfo, nullptr,
                    computePipeline.replace()) != VK_SUCCESS) {
            throw std::runtime_error("failed to create compute pipeline");
        }

        /*
         * create new comand pool since ideally compute and graphics are
         * seperate queues
         */
        VkCommandPoolCreateInfo cmdPoolInfo = {};
        cmdPoolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        cmdPoolInfo.queueFamilyIndex = queueFamilyIndices.compute;
        cmdPoolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

        if (vkCreateCommandPool(device, &cmdPoolInfo, nullptr,
                    computeCommandPool.replace()) != VK_SUCCESS) {
            throw std::runtime_error("failed to create compute command pool");
        }

        /* allocat associated command buffer */
        VkCommandBufferAllocateInfo cmdBufAllocInfo = {};
        cmdBufAllocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        cmdBufAllocInfo.commandPool = computeCommandPool;
        cmdBufAllocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        cmdBufAllocInfo.commandBufferCount = 1;

        if (vkAllocateCommandBuffers(device, &cmdBufAllocInfo,
                    &computeCommandBuffer) != VK_SUCCESS) {
            throw std::runtime_error("failed to allocate compute command buffer");
        }

        /* create fence to synchronize callbacks */
        VkFenceCreateInfo fenceCreateInfo = {};
        fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
        fenceCreateInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;
        if (vkCreateFence(device, &fenceCreateInfo, nullptr,
                    computeFence.replace()) != VK_SUCCESS) {
            throw std::runtime_error("failed to create compute fence");
        }
    }

    static VkWriteDescriptorSet writeDescriptorSet(VkDescriptorSet dstSet,
            VkDescriptorType type, uint32_t binding,
            VkDescriptorBufferInfo* bufferInfo, uint32_t descriptorCount = 1) {
        VkWriteDescriptorSet writeDescriptorSet {};

        writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writeDescriptorSet.dstSet = dstSet;
        writeDescriptorSet.descriptorType = type;
        writeDescriptorSet.dstBinding = binding;
        writeDescriptorSet.pBufferInfo = bufferInfo;
        writeDescriptorSet.descriptorCount = descriptorCount;

        return writeDescriptorSet;
    }

    void createComputeCommandBuffers() {
        /* create compute dispatch command */
        VkCommandBufferBeginInfo cmdBufInfo = {};
        cmdBufInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        if (vkBeginCommandBuffer(computeCommandBuffer, &cmdBufInfo)
                != VK_SUCCESS) {
            throw std::runtime_error("failed to begin compute command");
        }

        vkCmdBindPipeline(computeCommandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE,
                computePipeline);
        vkCmdBindDescriptorSets(computeCommandBuffer,
                VK_PIPELINE_BIND_POINT_COMPUTE, computePipelineLayout,
                0, 1, &computeDescriptorSet, 0, 0);
        vkCmdDispatch(computeCommandBuffer, ANTI_ALIASING * width / 8, ANTI_ALIASING * height / 8, 1);
        vkEndCommandBuffer(computeCommandBuffer);
    }

    void createCommandBuffers() {
        commandBuffers.resize(swapChainImages.size());

        VkCommandBufferAllocateInfo allocInfo = {};
        allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        allocInfo.commandPool = commandPool;
        allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        allocInfo.commandBufferCount = (uint32_t) commandBuffers.size();

        if (vkAllocateCommandBuffers(device, &allocInfo,
                    commandBuffers.data()) != VK_SUCCESS) {
            throw std::runtime_error("failed to allocated command buffers");
        }

        VkCommandBufferBeginInfo cmdBufInfo = {};
        cmdBufInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

        VkClearValue clearValues[2];
        clearValues[0].color = { {0.0f, 0.0f, 0.2f, 0.0f} };
        /* clearValues[1].depthStencil = { 1.0f, 0 }; */

        VkRenderPassBeginInfo renderPassBeginInfo = {};
        renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        renderPassBeginInfo.renderPass = renderPass;
        renderPassBeginInfo.renderArea.offset.x = 0;
        renderPassBeginInfo.renderArea.offset.y = 0;
        renderPassBeginInfo.renderArea.extent.width = width;
        renderPassBeginInfo.renderArea.extent.height = height;
        renderPassBeginInfo.clearValueCount = 1;
        renderPassBeginInfo.pClearValues = clearValues;

        for (uint32_t i = 0; i < commandBuffers.size(); i++) {
            // Set target frame buffer
            renderPassBeginInfo.framebuffer = swapChainFrameBuffers[i];

            if (vkBeginCommandBuffer(commandBuffers[i], &cmdBufInfo)
                    != VK_SUCCESS) {
                throw std::runtime_error("failed to begin command");
            }

            /*
             * image memory barrier to make sure that compute shader
             * writes are finished before sampling from the texture
             */
            VkImageMemoryBarrier imageMemoryBarrier = {};
            imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
            imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_GENERAL;
            imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_GENERAL;
            imageMemoryBarrier.image = textures[0]->image;
            imageMemoryBarrier.subresourceRange = {
                VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1
            };

            imageMemoryBarrier.srcAccessMask = VK_ACCESS_SHADER_WRITE_BIT;
            imageMemoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
            vkCmdPipelineBarrier(
                commandBuffers[i],
                VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
                VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
                0,
                0, nullptr,
                0, nullptr,
                1, &imageMemoryBarrier);

            vkCmdBeginRenderPass(commandBuffers[i], &renderPassBeginInfo,
                    VK_SUBPASS_CONTENTS_INLINE);

            VkViewport viewport = {};
            viewport.width = width;
            viewport.height = height;
            viewport.minDepth = 0.f;
            viewport.maxDepth = 1.f;

            vkCmdSetViewport(commandBuffers[i], 0, 1, &viewport);

            VkRect2D scissor = {};
            scissor.extent.width = width;
            scissor.extent.height = height;
            scissor.offset.x = 0;
            scissor.offset.y = 0;
            vkCmdSetScissor(commandBuffers[i], 0, 1, &scissor);

            /*
             * Display ray traced image generated by compute shader as a
             * full screen quad with vertices being configured in vertex shader
             */
            vkCmdBindDescriptorSets(commandBuffers[i],
                    VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipelineLayout,
                    0, 1, &graphicsDescriptorSet, 0, NULL);
            vkCmdBindPipeline(commandBuffers[i],
                    VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline);
            vkCmdDraw(commandBuffers[i], 3, 1, 0, 0);

            vkCmdEndRenderPass(commandBuffers[i]);

            if (vkEndCommandBuffer(commandBuffers[i]) != VK_SUCCESS) {
                throw std::runtime_error("failed to submit command");
            }
        }
    }

public:
    void render() {
        VkDeviceSize size = objects.size() * sizeof(Object);
        updateUniformBuffer(&objectBuffer, objects.data(), size);
        updateUniformBuffer(&cameraBuffer, &camera, sizeof(rayCamera));

        draw();
    }

private:
    void draw() {
        vkAcquireNextImageKHR(device, swapChain,
                std::numeric_limits<uint64_t>::max(), presentSemaphore,
                nullptr, &currentBuffer);

        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &commandBuffers[currentBuffer];
        if (vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE)
                != VK_SUCCESS) {
            throw std::runtime_error("failed to submit graphics command");
        }

        VkPresentInfoKHR presentInfo = {};
        presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
        presentInfo.swapchainCount = 1;
        presentInfo.pSwapchains = &swapChain;
        presentInfo.pImageIndices = &currentBuffer;
        presentInfo.waitSemaphoreCount = 1;
        presentInfo.pWaitSemaphores = &renderSemaphore;

        if (vkQueuePresentKHR(graphicsQueue, &presentInfo) != VK_SUCCESS) {
            throw std::runtime_error("failed");
        }

        if (vkQueueWaitIdle(graphicsQueue) != VK_SUCCESS) {
            throw std::runtime_error("failed to wait");
        }

        vkWaitForFences(device, 1, &computeFence, VK_TRUE,
                std::numeric_limits<uint64_t>::max());
        vkResetFences(device, 1, &computeFence);

        VkSubmitInfo computeSubmitInfo = {};
        computeSubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        computeSubmitInfo.commandBufferCount = 1;
        computeSubmitInfo.pCommandBuffers = &computeCommandBuffer;

        if (vkQueueSubmit(computeQueue, 1, &computeSubmitInfo, computeFence)
                != VK_SUCCESS) {
            throw std::runtime_error("failed to submit compute command");
        }
    }
};

void createPlane(glm::mat4 &T, glm::mat4 &Tinv, glm::vec3 trans, float rad,
        glm::vec3 axis, glm::vec3 scaleV) {
    T = glm::translate(T, trans);
    if (rad != 0) T = glm::rotate(T, rad, axis);
    T = glm::scale(T, scaleV);
    Tinv = glm::inverse(T);
}

int main(int argc, char **argv) {
    try {
        int width = 800;
        int height = 600;
        if( argc == 3 ) {
            width = atoi(argv[1]);
            height = atoi(argv[2]);
        }
        else if( argc != 1 ) {
            fprintf(stderr, "Usage: RayTracer [dimmensions]\n");
            exit(1);
        }

        RayTracer ray(width, height);
        Scene *scene = new Pong{*ray.window, ray.camera, &ray.objects};
        /* scene = new TestScene{ray.camera, &ray.objects}; */
        scene->initScene(ray.window);
        ray.initVulkan(scene->getTextures());

        /* fps stats */
        double fpsTimer = 0.0f;
        uint32_t frameCounter = 0;
        double diff = 33.3f;

        printf("\n");
        while (!glfwWindowShouldClose(ray.window)) {
            auto start = std::chrono::high_resolution_clock::now();

            glfwPollEvents();
            scene->updateScene(diff);
            ray.render();

            /* frame rate book keeping */
            frameCounter++;
            auto end = std::chrono::high_resolution_clock::now();
            diff = std::chrono::duration<double, std::milli>(end-start).count();
            fpsTimer += diff;
            if (fpsTimer > 1000.0f) {
                printf("\33[2K\rFPS: %3.2u\r", frameCounter);
                fflush(stdout);
                fpsTimer = 0.0f;
                frameCounter = 0;
            }
        }
    } catch (const std::runtime_error &e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    printf("\n");

    return EXIT_SUCCESS;
}
