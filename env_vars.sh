#!/bin/bash

export VULKAN_SDK=`pwd`/libs/VulkanSDK/1.0.42.1/x86_64

export PATH=$VULKAN_SDK/bin:$PATH
export LD_LIBRARY_PATH=$VULKAN_SDK/lib:$LD_LIBRARY_PATH
export VK_LAYER_PATH=$VULKAN_SDK/etc/explicit_layer.d
