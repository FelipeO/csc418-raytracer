/*
  CSC418 - RayTracer code - Winter 2017 - Assignment 3&4

  Written Dec. 9 2010 - Jan 20, 2011 by F. J. Estrada
  Freely distributable for adacemic purposes only.

  Uses Tom F. El-Maraghi's code for computing inverse
  matrices. You will need to compile together with
  svdDynamic.c

  You need to understand the code provided in
  this file, the corresponding header file, and the
  utils.c and utils.h files. Do not worry about
  svdDynamic.c, we need it only to compute
  inverse matrices.

  You only need to modify or add code in sections
  clearly marked "TO DO"
*/

#include "utils.h"

// A couple of global structures and data: An object list, a light list, and the
// maximum recursion depth
struct object3D *object_list;
struct pointLS *light_list;
int MAX_DEPTH;

void buildScene(void) {
    // Sets up all objects in the scene. This involves creating each object,
    // defining the transformations needed to shape and position it as
    // desired, specifying the reflectance properties (albedos and colours)
    // and setting up textures where needed.
    // Light sources must be defined, positioned, and their colour defined.
    // All objects must be inserted in the object_list. All light sources
    // must be inserted in the light_list.
    //
    // To create hierarchical objects:
    //   Copy the transform matrix from the parent node to the child, and
    //   apply any required transformations afterwards.
    //
    // NOTE: After setting up the transformations for each object, don't
    //       forget to set up the inverse transform matrix!

    struct object3D *o;
    struct pointLS *l;
    struct point3D p;

    ///////////////////////////////////////
    // TO DO: For Assignment 3 you have to use
    //        the simple scene provided
    //        here, but for Assignment 4 you
    //        *MUST* define your own scene.
    //        Part of your mark will depend
    //        on how nice a scene you
    //        create. Use the simple scene
    //        provided as a sample of how to
    //        define and position objects.
    ///////////////////////////////////////

    // Simple scene for Assignment 3:
    // Insert a couple of objects. A plane and two spheres
    // with some transformations.

    // Let's add a plane
    // Note the parameters: ra, rd, rs, rg, R, G, B, alpha, r_index, and
    // shinyness)

    // Note the plane is highly-reflective (rs=rg=.75) so we
    // should see some reflections if all is done properly.
    // Colour is close to cyan, and currently the plane is
    // completely opaque (alpha=1). The refraction index is
    // meaningless since alpha=1
    o = newPlane(.05, .75, .05, .05, .55, .8, .75, 1, 1, 2);
    Scale(o, 6, 6, 1); // Do a few transforms...
    RotateZ(o, PI / 1.20);
    RotateX(o, PI / 2.25);
    Translate(o, 0, -3, 10);
    // Very important! compute
    // and store the inverse
    // transform for this object!
    invert(&o->T[0][0], &o->Tinv[0][0]);
    // Insert into object list
    insertObject(o, &object_list);

    // Let's add a couple spheres
    o = newSphere(.05, .95, .35, .35, 1, .25, .25, 1, 1, 6);
    Scale(o, .75, .5, 1.5);
    RotateY(o, PI / 2);
    Translate(o, -1.45, 1.1, 3.5);
    invert(&o->T[0][0], &o->Tinv[0][0]);
    insertObject(o, &object_list);

    o = newSphere(.05, .95, .95, .75, .75, .95, .55, 1, 1, 6);
    Scale(o, .5, 2.0, 1.0);
    RotateZ(o, PI / 1.5);
    Translate(o, 1.75, 1.25, 5.0);
    invert(&o->T[0][0], &o->Tinv[0][0]);
    insertObject(o, &object_list);

    // Insert a single point light source.
    p.px = 0;
    p.py = 15.5;
    p.pz = -5.5;
    p.pw = 1;
    l = newPLS(&p, .95, .95, .95);
    insertPLS(l, &light_list);

    // End of simple scene for Assignment 3
    // Keep in mind that you can define new types of objects such as cylinders
    // and parametric surfaces,
    // or, you can create code to handle arbitrary triangles and then define
    // objects as surface meshes.
    //
    // Remember: A lot of the quality of your scene will depend on how much care
    // you have put into defining the relflectance properties of your objects,
    // and the number and type of light sources in the scene.
}

void rtShade(struct object3D *obj, struct point3D p, struct point3D n,
             struct ray3D ray, int depth, double a, double b,
             struct colourRGB *col) {
    // This function implements the shading model as described in lecture. It
    // takes
    // - A pointer to the first object intersected by the ray (to get the colour
    // properties)
    // - The coordinates of the intersection point (in world coordinates)
    // - The normal at the point
    // - The ray (needed to determine the reflection direction to use for the
    // global component, as well as for
    //   the Phong specular component)
    // - The current recursion depth
    // - The (a,b) texture coordinates (meaningless unless texture is enabled)
    //
    // Returns:
    // - The colour for this ray (using the col pointer)

    // Accumulator for colour components
    struct colourRGB tmp_col;
    // Colour for the object in R G and B
    double R, G, B;

    // This will hold the colour as we process all the components of
    // the Phong illumination model
    tmp_col.R = 0;
    tmp_col.G = 0;
    tmp_col.B = 0;

    if (obj->texImg == NULL) {
        // Not textured, use object colour
        R = obj->col.R;
        G = obj->col.G;
        B = obj->col.B;
    } else {
        // Get object colour from the texture given the texture coordinates
        // (a,b), and the texturing function
        // for the object. Note that we will use textures also for Photon
        // Mapping.
        obj->textureMap(obj->texImg, a, b, &R, &G, &B);
    }

    double use_ambient = 1, use_diffuse = 1, use_specular = 1, use_global = 1;

    for (struct pointLS *light = light_list; light; light = light->next) {
        // Add ambient components
        tmp_col.R += obj->alb.ra * light->col.R * use_ambient;
        tmp_col.G += obj->alb.ra * light->col.G * use_ambient;
        tmp_col.B += obj->alb.ra * light->col.B * use_ambient;

        // Calculate and add diffuse and specular components
        struct point3D light_dir = normalize(subVectors(p, light->p0));
        if( -dot(n, light_dir) < 0 && !obj->frontAndBack ) {
            // The normal is facing away from the light, and is one sided (e.g. the back of a sphere),
            // so we should get no lighting. Note that shadows don't handle this case since we exclude
            // self intersections to deal with rounding errors.

            continue;
        }

        // Detect if in shadow.
        double lambda;
        struct object3D *tmp_obj = NULL;
        struct point3D tmp_p, tmp_n;
        double tmp_a, tmp_b;
        findFirstHit(newRay(p, mulVector(light_dir, -1)), obj, &lambda, &tmp_obj, &tmp_p, &tmp_n, &tmp_a, &tmp_b);
        if( lambda >= 0 ) {
            // TODO: Make sure between, not just in direction of.

            // We are shadowed by some object, don't care what (we don't have transparency in a3)
            continue;
        }

        struct point3D reflection_dir = subVectors(mulVector(n, 2 * dot(n, light_dir)), light_dir);

        // Phong lighting model, using abs instead of max since if we are here either
        // a) It will be positive, as confirmed by an above if statement. So it's equivalent
        // b) We are supposed to lightBothSides, so abs is the correct model.
        tmp_col.R += obj->alb.rd * fabs(dot(n, light_dir)) * light->col.R * use_diffuse
                   + obj->alb.rs * pow(fabs(dot(ray.d, reflection_dir)), obj->shinyness) * light->col.R * use_specular;

        tmp_col.G += obj->alb.rd * fabs(dot(n, light_dir)) * light->col.G * use_diffuse
                   + obj->alb.rs * pow(fabs(dot(ray.d, reflection_dir)), obj->shinyness) * light->col.G * use_specular;


        tmp_col.B += obj->alb.rd * fabs(dot(n, light_dir)) * light->col.B * use_diffuse
                   + obj->alb.rs * pow(fabs(dot(ray.d, reflection_dir)), obj->shinyness) * light->col.B * use_specular;
    }

    // Calculate col (taking into acount local colour)

    col->R = tmp_col.R * R;
    col->G = tmp_col.G * G;
    col->B = tmp_col.B * B;

    // Add reflections (global component)
    struct point3D reflection_dir = addVectors(mulVector(n, -2 * dot(n, ray.d)), ray.d);
    struct colourRGB reflection_col = {-1, -1, -1};
    rayTrace(newRay(p, reflection_dir), depth + 1, obj, &reflection_col);
    if( reflection_col.R >= 0 ) {
        col->R += obj->alb.rg * reflection_col.R * use_global;
        col->G += obj->alb.rg * reflection_col.G * use_global;
        col->B += obj->alb.rg * reflection_col.B * use_global;
    }

    return;
}

void findFirstHit(struct ray3D ray, struct object3D *Os, double *lambda,
                  struct object3D **obj, struct point3D *p, struct point3D *n,
                  double *a, double *b) {
    // Find the closest intersection between the ray and any objects in the
    // scene.
    // It returns:
    //   - The lambda at the intersection (or < 0 if no intersection)
    //   - The pointer to the object at the intersection (so we can evaluate the
    //   colour in the shading function)
    //   - The location of the intersection point (in p)
    //   - The normal at the intersection point (in n)
    //
    // Os is the 'source' object for the ray we are processing, can be NULL, and
    // is used to ensure we don't
    // return a self-intersection due to numerical errors for recursive raytrace
    // calls.

    /////////////////////////////////////////////////////////////
    // TO DO: Implement this function. See the notes for
    // reference of what to do in here
    /////////////////////////////////////////////////////////////

    struct object3D *head = object_list;

    *lambda = INFINITY;
    double new_lambda;
    struct point3D new_p;
    struct point3D new_n;
    double new_a;
    double new_b;

    while (head) {
        if (head != Os) {
            head->intersect(head, ray, &new_lambda, &new_p, &new_n, &new_a, &new_b);
            if (0 <= new_lambda && new_lambda < *lambda) {
                *lambda = new_lambda;
                *p = new_p;
                *n = new_n;
                *a = new_a;
                *b = new_b;
                *obj = head;
            }
        }
        head = head->next;
    }

    if (*lambda == INFINITY) {
        *lambda = -1;
    }
}

void rayTrace(struct ray3D ray, int depth, struct object3D *Os,
              struct colourRGB *col) {
    // Ray-Tracing function. It finds the closest intersection between
    // the ray and any scene objects, calls the shading function to
    // determine the colour at this intersection, and returns the
    // colour.
    //
    // Os is needed for recursive calls to ensure that findFirstHit will
    // not simply return a self-intersection due to numerical
    // errors. For the top level call, Os should be NULL. And thereafter
    // it will correspond to the object from which the recursive
    // ray originates.

    double lambda;        // Lambda at intersection
    double a, b;          // Texture coordinates
    struct object3D *obj; // Pointer to object at intersection
    struct point3D p;     // Intersection point
    struct point3D n;     // Normal at intersection
    struct colourRGB I;   // Colour returned by shading function

    if (depth > MAX_DEPTH) {
         // Max recursion depth reached. Return invalid colour.

         // TODO: Make sure I do something with these (or return errors in some
         // sensible way!)
        col->R = -1;
        col->G = -1;
        col->B = -1;
        return;
    }

    ///////////////////////////////////////////////////////
    // TO DO: Complete this function. Refer to the notes
    // if you are unsure what to do here.
    ///////////////////////////////////////////////////////

    findFirstHit(ray, Os, &lambda, &obj, &p, &n, &a, &b);
    if (lambda < 0) {
        // No colision.

        // Return without modifying the colour, which should be set to background.
        // TODO: In the case of reflections, do we set to background or set to
        // the colour of the previous object? Or maybe colour of light?
        return;
    }
    rtShade(obj, p, n, ray, depth, a, b, col /*TODO: Switch back to &I*/);

    // TODO: Reflections

    //memcpy(col, &I, sizeof(struct colourRGB));
}

int main(int argc, char *argv[]) {
    // Main function for the raytracer. Parses input parameters,
    // sets up the initial blank image, and calls the functions
    // that set up the scene and do the raytracing.
    struct image *im; // Will hold the raytraced image
    struct view *cam; // Camera and view for this scene
    int sx;           // Size of the raytraced image
    int antialiasing; // Flag to determine whether antialiaing is enabled or
                      // disabled
    char output_name[1024]; // Name of the output file for the raytraced .ppm
                            // image
    struct point3D e;       // Camera view parameters 'e', 'g', and 'up'
    struct point3D g;
    struct point3D up;
    struct point3D du, dv; // Increase along u and v directions for pixel coordinates
    struct point3D d, d_row; // Point structures to keep the coordinates of the
                             // direction of a ray through the current pixel, and
                             // the start of the current row.
    struct ray3D ray;            // Structure to keep the ray from e to a pixel
    struct colourRGB col;        // Return colour for raytraced pixels
    struct colourRGB background; // Background colour
    unsigned char *rgbIm;

    if (argc < 5) {
        fprintf(stderr, "RayTracer: Can not parse input parameters\n");
        fprintf(stderr,
                "USAGE: RayTracer size rec_depth antialias output_name\n");
        fprintf(stderr, "   size = Image size (both along x and y)\n");
        fprintf(stderr, "   rec_depth = Recursion depth\n");
        fprintf(stderr, "   antialias = A single digit, 0 disables "
                        "antialiasing. Anything else enables antialiasing\n");
        fprintf(
            stderr,
            "   output_name = Name of the output file, e.g. MyRender.ppm\n");
        exit(0);
    }

    sx = atoi(argv[1]);
    MAX_DEPTH = atoi(argv[2]);
    if (atoi(argv[3]) == 0) {
        antialiasing = 0;
    } else {
        antialiasing = 1;
    }
    strcpy(&output_name[0], argv[4]);

    fprintf(stderr, "Rendering image at %d x %d\n", sx, sx);
    fprintf(stderr, "Recursion depth = %d\n", MAX_DEPTH);
    if (!antialiasing)
        fprintf(stderr, "Antialising is off\n");
    else
        fprintf(stderr, "Antialising is on\n");
    fprintf(stderr, "Output file name: %s\n", output_name);

    object_list = NULL;
    light_list = NULL;

    // Allocate memory for the new image
    im = newImage(sx, sx);
    if (!im) {
        fprintf(stderr, "Unable to allocate memory for raytraced image\n");
        exit(0);
    } else {
        rgbIm = (unsigned char *)im->rgbdata;
    }

    ///////////////////////////////////////////////////
    // TO DO: You will need to implement several of the
    //        functions below. For Assignment 3, you can use
    //        the simple scene already provided. But
    //        for Assignment 4 you need to create your own
    //        *interesting* scene.
    ///////////////////////////////////////////////////
    buildScene(); // Create a scene. This defines all the
                  // objects in the world of the raytracer

    //////////////////////////////////////////
    // TO DO: For Assignment 3 you can use the setup
    //        already provided here. For Assignment 4
    //        you may want to move the camera
    //        and change the view parameters
    //        to suit your scene.
    //////////////////////////////////////////

    // Mind the homogeneous coordinate w of all vectors below. DO NOT
    // forget to set it to 1, or you'll get junk out of the
    // geometric transformations later on.

    // Camera center is at (0,0,-3)
    e.px = 0;
    e.py = 0;
    e.pz = -3;
    e.pw = 1;

    // To define the gaze vector, we choose a point 'pc' in the scene that
    // the camera is looking at, and do the vector subtraction pc-e.
    // Here we set up the camera to be looking at the origin, so
    // g=(0,0,0)-(0,0,-3)
    g.px = 0;
    g.py = 0;
    g.pz = 3;
    g.pw = 0;

    // Define the 'up' vector to be the Y axis
    up.px = 0;
    up.py = 1;
    up.pz = 0;
    up.pw = 0;

    // Set up view with given the above vectors, a 4x4 window,
    // and a focal length of -1 (why? where is the image plane?)
    // Note that the top-left corner of the window is at (-2, 2)
    // in camera coordinates.
    cam = setupView(&e, &g, &up, -3, -2, 2, 4);

    if (cam == NULL) {
        fprintf(stderr, "Unable to set up the view and camera parameters. Our "
                        "of memory!\n");
        cleanup(object_list, light_list);
        deleteImage(im);
        exit(0);
    }

    // Set up background colour here
    background.R = 0;
    background.G = 0;
    background.B = 0;

    // Do the raytracing
    //////////////////////////////////////////////////////
    // TO DO: You will need code here to do the raytracing
    //        for each pixel in the image. Refer to the
    //        lecture notes, in particular, to the
    //        raytracing pseudocode, for details on what
    //        to do here. Make sure you undersand the
    //        overall procedure of raytracing for a single
    //        pixel.
    //////////////////////////////////////////////////////

    // du and dv. In the notes in terms of wl and wr, wt and wb.
    // We use wl, wt, and wsize. du=dv since the image is square
    // and dv is negative since y increases downward in pixel
    // coordinates and upward in camera coordinates.

    // We use a vector form and transform to world space here to
    // avoid needing to do extra expensive matrix calculations
    // inside the loop. This way finding the ray should compile
    // down to one simd add instructions inside the inner loop.

    du.px = cam->wsize / (sx - 1);
    du.py = 0;
    du.pz = 0;
    du.pw = 0;

    dv.px = 0;
    dv.py = - cam->wsize / (sx - 1);
    dv.pz = 0;
    dv.pw = 0;

    d_row.px = g.px + cam->wl;
    d_row.py = g.py + cam->wt;
    d_row.pz = g.pz;
    d_row.pw = 0;

    matVecMult(cam->C2W, &du);
    matVecMult(cam->C2W, &dv);
    matVecMult(cam->C2W, &d);

    fprintf(stderr, "View parameters:\n");
    fprintf(stderr, "Left=%f, Top=%f, Width=%f, f=%f\n", cam->wl, cam->wt,
            cam->wsize, cam->f);
    fprintf(stderr,
            "Camera to world conversion matrix (make sure it makes sense!):\n");
    printmatrix(cam->C2W);
    fprintf(stderr, "World to camera conversion matrix\n");
    printmatrix(cam->W2C);
    fprintf(stderr, "\n");

    fprintf(stderr, "Rendering Row:");
    for (int y = 0; y < im->sy; y++) {
        // For each of the pixels in the image

        fprintf(stderr, "%d/%d, ", y, im->sy);

        d = d_row;
        for (int x = 0; x < im->sx; x++) {
            ///////////////////////////////////////////////////////////////////
            // TO DO - complete the code that should be in this loop to do the
            //         raytracing!
            ///////////////////////////////////////////////////////////////////
            ray = newRay(e, normalize(d));

            col = background;
            rayTrace(ray, 0, NULL, &col);

            setColour(im, x, y, 0, col.R);
            setColour(im, x, y, 1, col.G);
            setColour(im, x, y, 2, col.B);

            d = addVectors(du, d);
        }

        d_row = addVectors(dv, d_row);
    }

    fprintf(stderr, "\nDone!\n");

    // Output rendered image
    imageOutput(im, output_name);

    // Exit section. Clean up and return.
    cleanup(object_list, light_list); // Object and light lists
    deleteImage(im);                  // Rendered image
    free(cam);                        // camera view
    exit(0);
}
