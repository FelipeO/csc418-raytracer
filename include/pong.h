#ifndef PONG_H
#define PONG_H

#include "raytracer.h"
#include "scene.h"
#include "ball.h"
#include "settings.h"
#include "glm/gtx/transform.hpp"


void pongControlCallback(GLFWwindow *window, int key, int scancode, int action,
        int mods);

class Pong : public Scene {
private:
    GLFWwindow &window;
    std::vector<Object> *buffer;

    bool paused;

    Object lense;
    std::vector<Object> lights;

    /* game ball */
    Ball ball {window};

    /* quadtrileral paddles */
    std::array<Object, 4> left;
    std::array<Object, 4> right;
    std::array<Object, 6> surroundings;

public:
    Pong(GLFWwindow &window, struct rayCamera &camera,
     std::vector<Object> *objects);

    /* external interface */
    void initScene(GLFWwindow *window);
    void updateScene(double dt);
    std::vector<std::pair<uint32_t, const char *>> getTextures();

    void controlCallback(GLFWwindow *window, int key, int scandcode,
            int action, int mods);

private:
    void createGame();
    void createLights();
    void createPaddle(std::array<Object, 4> &paddle, float reflection);
    void createSurroundings();
    void updatePaddles(double dt);
    void updatePaddle(std::array<Object, 4> &paddle, bool move, double dt);
    void updateBuffer();
};

#include <unistd.h>
#endif
