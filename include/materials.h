#ifndef MATERIALS_H
#define MATERIALS_H

extern const std::vector<struct material> materials;

struct material {
    glm::vec3 diffuse;
    float specular;
};

/* different types of materials */
enum Material {
    MATTE,
    GLOSSY,
    MIRROR
};

#endif
