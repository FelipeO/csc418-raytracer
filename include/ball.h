#ifndef BALL_H
#define BALL_H

#include "raytracer.h"
#include "glm/gtx/transform.hpp"
#include "settings.h"


enum BallType {
    // Just a distortion ball
    Refracty2,
    // Start here.
    // Non-Transparent
    TexturedBall1,
    // Glowing ball
    LightSource1,
    // Solar system...
    SolarBall,
    MaxBallType,
};

class Ball {
public:
    float radius;
private:
    glm::vec3 pos;
    GLFWwindow &window;

    float ball_move_speed;
    glm::vec3 ballDir;

    glm::mat4 squashMatrix;
    float outer_radius;

    BallType type;
public:
    Ball(GLFWwindow &window);
    std::vector<Object> objects();
    void reset();
    bool update(bool paused, double dt, float left_paddle, float right_paddle);
    void intersect(float xSign, float ballX, float ballY);
private:
    // void scale(glm::vec3 v);
    void updateSpeed();
    void next_type();
};

#endif