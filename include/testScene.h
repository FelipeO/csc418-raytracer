#ifndef TEST_SCENE_H
#define TEST_SCENE_H

#include "raytracer.h"
#include "scene.h"

class TestScene : public Scene {
public:
    TestScene(struct rayCamera &camera, std::vector<Object> *objects) {
        glm::vec3 eyePos = glm::vec3(0.0, 0.0, -3.0);
        glm::mat4 camera2world = glm::inverse(glm::lookAt(
            eyePos,
            eyePos + glm::vec3(0.0, 0.0, 1.0),
            glm::vec3(0.0, 1.0, 0.0)
        ));

        camera = {
            camera2world,
            eyePos,
            1.5
        };

        (*objects)[0] = {
            glm::vec3(0.95f, 0.95f, 0.95f),
            0,
            { 0.0f, 0.0f, 0.0f, 0.0f },
            glm::mat4(),
            glm::mat4(),
            1.0f,
            6,
            0.0,
            OBJECT_LIGHT
        };
        (*objects)[0].T = glm::translate((*objects)[0].T,
                glm::vec3(0.0f, 15.5f, -5.5f));
        (*objects)[0].Tinv = glm::inverse((*objects)[0].T);

        (*objects)[1] = {
            glm::vec3(1.0f, 0.25f, 0.25f),
            1,
            { 0.05f, 0.95f, 0.35f, 0.35 },
            glm::mat4(),
            glm::mat4(),
            0.0f,
            6,
            0.0,
            OBJECT_SPHERE
        };
        (*objects)[1].T = glm::translate((*objects)[1].T,
                glm::vec3(-1.45, 1.1, 3.5));
        (*objects)[1].T = glm::rotate((*objects)[1].T , (float) (PI / 2.0),
                glm::vec3(0.0, 1.0, 0.0));
        (*objects)[1].T = glm::scale((*objects)[1].T, glm::vec3(0.75f, 0.5f, 1.5f));
        (*objects)[1].Tinv = glm::inverse((*objects)[1].T);

        (*objects)[2] = {
            glm::vec3(0.75f, 0.95f, 0.55f),
            2,
            { 0.05f, 0.95f, 0.35f, 0.35f },
            glm::mat4(),
            glm::mat4(),
            0.0f,
            6,
            0.0,
            OBJECT_SPHERE
        };
        (*objects)[2].T = glm::translate((*objects)[2].T,
                glm::vec3(1.75f, 1.25f, 5.0f));
        (*objects)[2].T = glm::rotate((*objects)[2].T, (float) (PI / 1.5),
                glm::vec3(0.0f, 0.0f, 1.0f));
        (*objects)[2].T = glm::scale((*objects)[2].T,
                glm::vec3(0.5f, 2.0f, 1.0f));
        (*objects)[2].Tinv = glm::inverse((*objects)[2].T);

        (*objects)[3] = {
            glm::vec3(0.55f, 0.8f, 0.75f),
            2,
            { 0.05f, 0.75f, 0.05f, 0.05f },
            glm::mat4(),
            glm::mat4(),
            0.0f,
            2,
            0.0,
            OBJECT_PLANE
        };

        (*objects)[3].T = glm::translate((*objects)[3].T,
                glm::vec3(0, -3, 10));
        (*objects)[3].T = glm::rotate((*objects)[3].T, PI / 2.25f,
                glm::vec3(1, 0, 0));
        (*objects)[3].T = glm::rotate((*objects)[3].T, PI / 1.20f,
                glm::vec3(0, 0, 1));
        (*objects)[3].T = glm::scale((*objects)[3].T,
                glm::vec3(6, 6, 1));
        (*objects)[3].Tinv = glm::inverse((*objects)[3].T);
    }

    void initScene(GLFWwindow *) { return; }

    void updateScene(double dt) { return; }
    std::vector<std::pair<uint32_t, const char *>> getTextures() {
        return std::vector<std::pair<uint32_t, const char *>>();
    }
};

#endif
