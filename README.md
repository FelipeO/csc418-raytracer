![Sample Video](gameplay.mp4)

# To clone

```
git clone https://gitlab.com/gmorenz/csc418-raytracer
git submodule init
git submodule update
```

# Build instructions

1. Change into this directory!
2. `source env_vars.sh`
3. `mkdir build`
4. `cd build`
5. `cmake ..`
6. `make`
7. `./RayTracer` (or `./RayTracer width height`)

We have only tested on linux. We don't know of any reason why this wouldn't work on
windows (provided you replaced the VulkanSDK in libs with a windows version),
but we can't guarantee it will. OSX does not support vulkan.

# Enabling antialising

Anti-aliasing can be enabled by:

1. Setting `ANTI_ALIASING` in `include/settings.h` to `2`.
2. Uncommenting `#define ANTI_ALIASING` in `shader.comp`.

Anti-aliasing can be disabled by:

1. Setting `ANTI_ALIASING` in `include/settings.h` to `1`.
2. Commenting out `#define ANTI_ALIASING` in `shader.comp`.

Anti-aliasing comes with a approximately 4x performance penalty, which is unsurprising since
we are rendering 4x the number of pixels.

# Running on non-NVIDIA GPUs

We are using an extension to load GLSL code directly on NVidia GPUs instead of
compiled SPIR-V code since it leads to a large performance improvement (roughly 25%).

To run on a non-NVIDIA GPU

1. Uncomment`#define GLSL_SHADER` at the top of `src/raytracer.cpp`.
2. Run`make -C shaders`.